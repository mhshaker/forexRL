
import numpy as np
import math
import tensorflow as tf
from tensorflow.contrib import rnn
import tf_util as util
import os, os.path, time, math, random
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
random.seed(1)
np.random.seed(1)
tf.set_random_seed(1)
np.set_printoptions(threshold=np.nan)
n_nodes = 50

class PolicyGradient:
    def __init__(
            self,
            n_actions,
            n_features,
            seq_len,
            learning_rate=0.01,
            reward_decay=0.99,
            dropout=0.9,
            writer_name="",
            run_model=0
    ):
        self.run_model = run_model
        self.dropout = dropout
        self.step = 0
        self.learning_rate = learning_rate
        tf.reset_default_graph()

        with tf.name_scope('inputs'):
            self.observation = tf.placeholder(tf.float32, [None, seq_len, n_features], name='observation')
            self.rewards = tf.placeholder(tf.float32, [None, 1], name='reward')  
            self.seq_len = seq_len
            # self.labels = tf.placeholder(tf.int64, [None, 1], name='labels')  
            self.prewards = tf.placeholder(tf.float32, [None, 1], name='pip_reward')  
            self.actions = tf.placeholder(tf.int32, [None,], name='action') 
            # self.positions = tf.placeholder(tf.float32, [None,], name='position') 
            self.pkeep = tf.placeholder(tf.float32, name='dropout')
            self.global_step = tf.placeholder(tf.int32, name='global_step')  # learning rate
  
            # self.lr = tf.placeholder(tf.float32, name='lr')
            # self.global_step = tf.placeholder(tf.float32, name='step')

        with tf.variable_scope('model'):
            
            l = util.denseLayer(self.observation, 50, self.pkeep, "dense1")
            if self.run_model >= 2:
                l = util.denseLayer(l, 50, self.pkeep, "dense2")
            if self.run_model >= 3:
                l = util.denseLayer(l, 50, self.pkeep, "dense3")

            # l = util.denseLayer(l, 30, self.pkeep, "dense3")
            # l = util.denseLayer(l, 20, self.pkeep, "dense4")
            # l = util.denseLayer(l, 20, self.pkeep, "dense5")


            # l = util.convLayer(self.observation, 50, 3, "conv1", self.pkeep)
            # if self.run_model >= 2:
            #     l = util.convLayer(l, 50, 3, "conv2", self.pkeep)
            # if self.run_model >= 3:
            #     l = util.convLayer(l, 50, 3, "conv3", self.pkeep)

            # self.l = tf.reshape(l, [-1, 50*self.seq_len]) 
            self.l = l[:, -1]
            self.logits = tf.layers.dense(self.l, #l[:, -1], #[batch, feature]
                n_actions, 
                activation=None, 
                bias_initializer=tf.constant_initializer(0.1),
                kernel_initializer=tf.contrib.layers.xavier_initializer(seed=1), 
                name='logits') # l[:, -1]

            self.action = tf.nn.softmax(self.logits)
            self.sample_op = tf.multinomial(logits=self.logits,
                                            num_samples=1, 
                                            seed= 1, 
                                            name="sample_op")


        with tf.variable_scope('train'):
            self.one_hot_actions = tf.one_hot(self.actions,n_actions)
            self.neg_log_prob = tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, 
                labels=tf.one_hot(self.actions,n_actions))

            # self.neg_log_prob = self.one_hot_actions * tf.log(self.action)

            # self.rewards = self.rewards / 10 
            self.loss = tf.reduce_mean(self.rewards * (self.neg_log_prob + 1E-1)) * 1000 
            # lr = tf.train.exponential_decay(learning_rate, global_step=self.global_step, decay_steps=-40000, decay_rate=math.e)
            
            # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            # with tf.control_dependencies(update_ops):
            # lr = tf.train.exponential_decay(learning_rate, global_step=self.global_step, decay_steps=-100000, decay_rate=math.e)
            self.train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.loss)
            loss_summary = tf.summary.scalar("loss", self.loss)
            # lr_summary = tf.summary.scalar("learning_rate", lr)

            # lr_summary = tf.summary.scalar("lr", lr)


        with tf.variable_scope('performance_main'):
            # reward_summary = tf.summary.scalar("rewards", tf.reduce_mean(self.rewards))
            reward_summary = tf.summary.scalar("pip_rewards", tf.reduce_sum(self.prewards))

            # is_correct = tf.equal(self.labels, self.action)
            # accuracy = tf.reduce_mean(tf.cast(is_correct, tf.float32))
            # accuracy_summary = tf.summary.scalar("batch_accuracy", accuracy)

            # correctAction = tf.cast(is_correct, tf.float32)
            # correctPip = correctAction * tf.abs(self.rewards)
            # pipProfit = tf.reduce_sum(correctPip)
            # incorrectAction = tf.cast(tf.logical_not(is_correct), tf.float32)
            # incorrectPip = incorrectAction * tf.abs(self.rewards)
            # pipLoss = tf.reduce_sum(incorrectPip)
            # batch_profit = (pipProfit - pipLoss)
            # r = pipProfit/pipLoss
            # kelly = (accuracy - ((1-accuracy)/r) ) * 100
            # r_summary = tf.summary.scalar("batch_R",r)
            # kelly_summary = tf.summary.scalar("batch_Kelly",kelly)  


        
        with tf.variable_scope('util'):
            for var in tf.trainable_variables():
                # print("var.name: ",var.name)
                tf.summary.histogram(var.name, var)
                # var_grad = tf.reduce_mean(tf.gradients(self.loss, var))
                # grad_summary = tf.summary.scalar(var.name+"gradient", tf.reduce_mean(var_grad))
                # mean = tf.reduce_mean(var)
                # tf.summary.scalar((var.name+'mean'), mean)
                # with tf.name_scope('stddev'):
                #     stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
                # tf.summary.scalar((var.name+'stddev'), stddev)
                # tf.summary.scalar((var.name+'max'), tf.reduce_max(var))
                # tf.summary.scalar((var.name+'min'), tf.reduce_min(var))
                # tf.summary.histogram((var.name+'histogram'), var)
        


        self.summaries = tf.summary.merge_all()

        self.sess = tf.InteractiveSession()

        self.summary_writer = tf.summary.FileWriter("logs/" + writer_name, self.sess.graph)
        self.validation_writer = tf.summary.FileWriter("logs/" + writer_name + "_validRandom", self.sess.graph)
        # self.validation_writer_argmax = tf.summary.FileWriter("logs/" + writer_name + "_validArgmax", self.sess.graph)
        if not os.path.exists("checkpoints"):
            os.mkdir("checkpoints")
        self.saver = tf.train.Saver(max_to_keep=1000)

        tf.global_variables_initializer().run()

    def learn_p(self, o, a, r, p=0,l=0, batch_size=0, online=False):
        # replay memmory
        if batch_size != 0:
            idx = np.random.choice(np.arange(len(a)), batch_size, replace=False)
            o = o[idx]
            a = a[idx]
            r = r[idx]

        # discounted_r = self.discount_rewards(r)
        rewards = np.array(r).reshape(-1,1)
        # labels = np.array(l).reshape(-1,1)
        # positions = np.array(p).reshape(-1,1)
        pip_rewards = np.array(r).reshape(-1,1)

        feed_dict = feed_dict={
            self.observation: o,
            self.actions: a,
            self.rewards: rewards,
            # self.labels: labels,
            # self.positions: positions,
            self.prewards: pip_rewards,
            self.pkeep: self.dropout,
            # self.lr: self.learning_rate,
            self.global_step: self.step
        }

        _, smm, a,l, clog= self.sess.run([self.train_op, self.summaries, self.one_hot_actions, self.logits, self.neg_log_prob], feed_dict=feed_dict)
        
        # print("data",sl)
        # print("slice data",sls)
        # exit()

        # print("runtime crossEntropy",clog)
        # print("runtime reward", rewards)
        # exit()
        if self.step % 100 == 0 and not online:
            self.summary_writer.add_summary(smm, self.step)
        if online == True:
            self.validation_writer.add_summary(smm, self.step)

        self.step += 1

    
    def valid_p(self, o, a, r, p=0, l=0,summ_name="random"):
        # discounted_r = self.discount_rewards(r)
        rewards = np.array(r).reshape(-1,1)
        pip_rewards = np.array(r).reshape(-1,1)
        # labels = np.array(l).reshape(-1,1)
        # positions = np.array(p).reshape(-1,1)

        smm = self.sess.run(
            self.summaries,
            feed_dict={
                self.observation: o,
                self.actions: a,
                self.rewards: rewards, # self.memory[:, self.n_features + 1],
                self.prewards: pip_rewards,
                # self.labels: labels,
                # self.positions: positions,
                self.pkeep: 1,
                self.global_step: self.step
            })
        if summ_name == "random":
            self.validation_writer.add_summary(smm, self.step)
        elif summ_name == "argmax":
            self.validation_writer_argmax.add_summary(smm, self.step)

   
    def discount_rewards(self, rewards, gamma=0.99):
        discounted_r = np.zeros_like(rewards,dtype=np.float32)
        counter = 1
        for t in reversed(range(0, len(rewards))):
            if rewards[t] != 0:
                discounted_r[t] = rewards[t]
                counter = 1
            else:
                # print("reseting discount")
                if t+1 < len(rewards):
                    discounted_r[t] = discounted_r[t+1] * (gamma)
                else:
                    discounted_r[t] = rewards[t]
                counter += 1

        # discounted_r = np.zeros_like(rewards,dtype=np.float32)
        # counter = 1
        # for t in range(0, len(rewards)):
        #     gt = 0
        #     for x in range(t,len(rewards)):
        #         gt += rewards[x]
        #     discounted_r[t] = gt
        
        # normalize episode rewards
        # discounted_r -= np.mean(discounted_r)
        # discounted_r /= np.std(discounted_r) + 0.000001
        return discounted_r
    
    # def normalize_obserbatino(self, observation):
    #     # print(observation)
    #     # return np.exp(observation)
    #     return (observation - np.mean(observation) / (np.std(observation) + 1E-7))

    # def convert_to_return(self, observation):
    #     s = np.roll(observation, 1)
    #     observation = observation - s
    #     observation = observation[1:]
    #     return observation


    def choose_action_one(self, observation):
        observation = observation[np.newaxis, :]
        action, logits = self.sess.run([self.sample_op, self.logits], feed_dict={self.observation: observation, self.pkeep: 1})
        return action[0][0]

    def choose_action_batch(self, observation):
        action, logits = self.sess.run([self.sample_op, self.logits], feed_dict={self.observation: observation, self.pkeep: 1})
        action = np.reshape(action, -1)
        return action

    def choose_action_batch_noSample_op(self, observation):
        # positions = np.array(positions).reshape(-1,1)
        action = self.sess.run([self.action], feed_dict={self.observation: observation,self.pkeep: 1})
        # action = np.reshape(action, -1)
        res = []
        action = np.reshape(action, [-1,2])
        # print(action.shape)
        for x in action:
            # print('probobilitys in choose action:',x)
            res.append(np.random.choice([0,1],1,p=x))
        res = np.array(res)
        res = np.reshape(res, -1)
        # print(res)
        return np.array(res)

    def choose_action_batch_test(self, observation):
        action, logits = self.sess.run([self.action, self.logits], feed_dict={self.observation: observation, self.pkeep: 1})
        # print("choose_action", action)
        action = np.reshape(action, -1)
        return action