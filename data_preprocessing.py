import pandas as pd
from tqdm import tqdm
data = pd.read_csv('../Data/EURUSD15_spline_K4T4A4B4C3n5000_m.csv')
print(data.tail())

# Heiken Ashi 
data['xClose'] = (data.open + data.high + data.low + data.close) / 4
data['xOpen']  = (data.open.shift(-1) + data.close.shift(-1)) / 2
for x in tqdm(range(1,len(data))):
    data['xOpen'].iloc[x] = (data.xOpen.iloc[x-1] + data.xClose.iloc[x-1]) / 2
data["xHigh"] = data[["high", "xOpen","xClose"]].max(axis=1)
data["xLow"] = data[["low", "xOpen","xClose"]].min(axis=1)
print("Heiken Ashi Done.")

# moving average

data['MA5'] = data['close'].rolling(window=5, min_periods=0).mean()
data['MA20'] = data['close'].rolling(window=20, min_periods=0).mean()
data['MA50'] = data['close'].rolling(window=50, min_periods=0).mean()
print("moving average Done.")

# spline high and low dif

data['H_line_1'] = data.line_1 - data.high
data['H_line_2'] = data.line_2 - data.high
data['H_line_3'] = data.line_3 - data.high
data['H_line_4'] = data.line_4 - data.high
data['H_line_5'] = data.line_5 - data.high
data['H_line_6'] = data.line_6 - data.high
data['H_line_7'] = data.line_7 - data.high
data['H_line_8'] = data.line_8 - data.high

data['L_line_1'] = data.line_1 - data.low
data['L_line_2'] = data.line_2 - data.low
data['L_line_3'] = data.line_3 - data.low
data['L_line_4'] = data.line_4 - data.low
data['L_line_5'] = data.line_5 - data.low
data['L_line_6'] = data.line_6 - data.low
data['L_line_7'] = data.line_7 - data.low
data['L_line_8'] = data.line_8 - data.low

data['abs_H_line_1'] = abs(data.line_1 - data.high)
data['abs_H_line_2'] = abs(data.line_2 - data.high)
data['abs_H_line_3'] = abs(data.line_3 - data.high)
data['abs_H_line_4'] = abs(data.line_4 - data.high)
data['abs_H_line_5'] = abs(data.line_5 - data.high)
data['abs_H_line_6'] = abs(data.line_6 - data.high)
data['abs_H_line_7'] = abs(data.line_7 - data.high)
data['abs_H_line_8'] = abs(data.line_8 - data.high)

data['abs_L_line_1'] = abs(data.line_1 - data.low)
data['abs_L_line_2'] = abs(data.line_2 - data.low)
data['abs_L_line_3'] = abs(data.line_3 - data.low)
data['abs_L_line_4'] = abs(data.line_4 - data.low)
data['abs_L_line_5'] = abs(data.line_5 - data.low)
data['abs_L_line_6'] = abs(data.line_6 - data.low)
data['abs_L_line_7'] = abs(data.line_7 - data.low)
data['abs_L_line_8'] = abs(data.line_8 - data.low)
print("spline Done.")


# clean unwanted data

data = data[['Date',
         'time',
         'open',
         'high',
         'low',
         'volume',    
         'xOpen',  
         'xHigh',
         'xLow',
         'xClose',  
         'MA5',
         'MA20',
         'MA50',
         'Tenkan_sen',
         'Kijun_sen',
         'Span_A',
         'Span_B',
         'Chikou_Span',
         'line_1',
         'line_2',
         'line_3',
         'line_4',
         'line_5',
         'line_6',
         'line_7',
         'line_8',
         'H_line_1',
         'H_line_2',
         'H_line_3',
         'H_line_4',
         'H_line_5',
         'H_line_6',
         'H_line_7',
         'H_line_8',
         'L_line_1',
         'L_line_2',
         'L_line_3',
         'L_line_4',
         'L_line_5',
         'L_line_6',
         'L_line_7',
         'L_line_8', 
         'abs_H_line_1',
         'abs_H_line_2',
         'abs_H_line_3',
         'abs_H_line_4',
         'abs_H_line_5',
         'abs_H_line_6',
         'abs_H_line_7',
         'abs_H_line_8',
         'abs_L_line_1',
         'abs_L_line_2',
         'abs_L_line_3',
         'abs_L_line_4',
         'abs_L_line_5',
         'abs_L_line_6',
         'abs_L_line_7',
         'abs_L_line_8',
         'close',
        ]]  
data = data.round(5)

print("cleaning Done.")

data.to_csv('../Data/EURUSD15_pRL.csv')
print("All Done.")
