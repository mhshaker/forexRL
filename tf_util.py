
import tensorflow as tf
import random
import numpy as np
np.random.seed(1)
random.seed(1)
tf.set_random_seed(1)


def make_seq(sess, x, seq_len, n_features):
    N = int(sess.run(tf.size(x)) / n_features)
    print("tf.size",N)
    windows = [tf.slice(x, [b,0], [seq_len,n_features]) for b in range(0, N-seq_len)]
    windows = tf.stack(windows)
    return windows

def denseLayer(input, nodes, pkeep, name):
    with tf.variable_scope(name):
        l = tf.layers.dense(
                input, 
                nodes, 
                activation=tf.nn.relu,
                use_bias=False, 
                bias_initializer=tf.constant_initializer(0.1),
                kernel_initializer=tf.contrib.layers.xavier_initializer(seed=1), # tf.random_normal_initializer(mean=0, stddev=0.1)
                name=name
            )
        
        # l = tf.layers.batch_normalization(l)
        l = tf.nn.dropout(l, pkeep)
        return l

def convLayer(input, filters, kernel , name, pkeep):
    with tf.variable_scope(name):
        cl = tf.layers.conv1d(
            input, filters=filters,  
            kernel_size=kernel, 
            kernel_initializer=tf.contrib.layers.xavier_initializer(seed=1),
            padding='SAME', 
            activation=None, 
            name=name)                
        # cl = tf.layers.batch_normalization(cl)
        cl = tf.nn.relu(cl)
        # cl = tf.nn.dropout(cl, pkeep, name="dropout_"+name)
        return cl

def squeeze_layer(input, sq_nodes, fire_size, name):
    with tf.variable_scope(name):
        s = convLayer(input, sq_nodes, 1, "squeeze")
        l1 = convLayer(s, fire_size, 3, "conv3")
        l2 = convLayer(s, fire_size, 1, "conv1")
        l = tf.concat([l1, l2], 2)
        return l

def attention(inputs, seq_length):
    with tf.variable_scope('attention'):
        # print("inputs.shape ",inputs.shape)
        hidden_size = inputs.shape[2].value
        print("att.hidden_size",hidden_size)
        print("att.seq_length",seq_length)
        print("att.input",inputs.shape)
        # print(inputs.shape, " hidden_size:", hidden_size)
        l = tf.reshape(inputs, [-1, hidden_size])
        l = tf.layers.dense(l, hidden_size, activation=tf.nn.relu,
                            bias_initializer=tf.constant_initializer(0.1),
                            kernel_initializer=tf.contrib.layers.xavier_initializer(seed=1))
        logits = tf.layers.dense(l,1, activation=None,
                                bias_initializer=tf.constant_initializer(0.1),
                                kernel_initializer=tf.contrib.layers.xavier_initializer(seed=1))
        logits = tf.reshape(logits, [-1, seq_length,1])
        alphas = tf.nn.softmax(logits)
        # print("alphas.shape ",alphas.shape)
        # print("inputs.shape ",inputs.shape)
        encoding = tf.reduce_mean(inputs * alphas, 1)
        print("encoding.shape",encoding.shape)
        return encoding, alphas

def make_hparam_string(learning_rate, dropout):
    return "_lr%.0E_pkeep%s" % (learning_rate, dropout)

def mapIndexToAction(index):
    with tf.variable_scope("mapIndexToAction"):
        def helper(): return tf.cond(tf.equal(index, tf.constant(1,dtype=tf.int64)),isSell,isNon)
        def isBuy():  return 1.0
        def isSell(): return -1.0
        def isNon():  return 0.0
        return tf.cond(tf.equal(index, tf.constant(0,dtype=tf.int64)),isBuy,helper)

def get_spread(action, last_action, spread):
    def _spread(action, last_action):
        s = 0
        if (last_action == 1 and action == -1) or (last_action == -1 and action == 1) or (last_action == 0 and action != last_action):
            s = spread
        return s
    return list(map(_spread, action, last_action))

def make_positive_reward(reward):
    with tf.variable_scope("make_positive_reward"):
        return tf.cond(tf.greater(reward, tf.constant(0,dtype=tf.float32)),lambda: reward,lambda: 1.0)

def make_negative_reward(reward):
    with tf.variable_scope("make_negative_reward"):
        return tf.cond(tf.less(reward, tf.constant(0,dtype=tf.float32)),lambda: tf.abs(reward),lambda: 1.0)


def reward(action, price_, spread):
    with tf.variable_scope("reward"):
        action = tf.map_fn(mapIndexToAction, action, dtype=tf.float32, name="action_map")
        reward = ((price_ * action) - spread) * 10000
        p_reward = tf.map_fn(make_positive_reward, reward, dtype=tf.float32, name="p_reward")
        n_reward = tf.map_fn(make_negative_reward, reward, dtype=tf.float32, name="n_reward")

        return reward, p_reward, n_reward



def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization)."""
    with tf.name_scope('Variable_Summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar((name+'mean'), mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar((name+'stddev'), stddev)
        tf.summary.scalar((name+'max'), tf.reduce_max(var))
        tf.summary.scalar((name+'min'), tf.reduce_min(var))
        tf.summary.histogram((name+'histogram'), var)

def getData(Variable_Summaries, p=0.8):
    Variable_Summaries = Variable_Summaries/5
    negative_reward = np.random.choice([1,-1],1000,p=[p,1-p])
    Variable_Summaries = Variable_Summaries * negative_reward
    return Variable_Summaries





# <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<
def build_multi_dynamic_brnn(num_layer, RNN_CELL_SIZE, activation, batch_size, seqLengths, inputX, cell_fn, \
                             dropout_enabled=True, dropout_keep_prob_value=0.8):
    fbHrs = None
    hid_input = inputX
    for i in range(num_layer):
        scope = 'DBRNN_' + str(i + 1)
        # forward_cell = cell_fn(num_hidden, activation=activation)
        forward_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        # backward_cell = cell_fn(RNN_CELL_SIZE, activation=activation)
        backward_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        # tensor of shape: [batch_size, max_time, input_size]
        outputs, output_states = tf.nn.bidirectional_dynamic_rnn(forward_cell, backward_cell,
                                                                 inputs=hid_input,
                                                                 dtype=tf.float32,
                                                                 # sequence_length=seqLengths,
                                                                 scope=scope)
        # forward output, backward ouput
        # tensor of shape: [batch_size, max_time, input_size]
        output_fw, output_bw = outputs
        # forward states, backward states
        output_state_fw, output_state_bw = output_states
        # output_fb = tf.concat(2, [output_fw, output_bw])
        output_fb = tf.concat([output_fw, output_bw], 2)
        shape = output_fb.get_shape().as_list()
        output_fb = tf.reshape(output_fb, [shape[0], shape[1], 2, int(shape[2] / 2)])
        hidden = tf.reduce_sum(output_fb, 2)
        if dropout_enabled:
            hidden = tf.nn.dropout(hidden, dropout_keep_prob_value)

        if i != num_layer - 1:
            hid_input = hidden
        else:
            # outputXrs = tf.reshape(hidden, [-1, RNN_CELL_SIZE])
            # # output_list = tf.split(0, seqLengths, outputXrs)
            # output_list = tf.split(outputXrs, seqLengths, 0)
            # fbHrs = [tf.reshape(t, [batch_size, RNN_CELL_SIZE]) for t in output_list]

            fbHrs = hidden

    return fbHrs


def build_multi_dynamic_brnn_2(X_data_numerical, training, RNN_CELL_SIZE=128, state=None, bidirectional_flag=True):
    # time_major: The shape format of the `inputs` and `outputs` Tensors.
    #   If true, these `Tensors` must be shaped `[max_time, batch_size, depth]`.
    #   If false, these `Tensors` must be shaped `[batch_size, max_time, depth]`.
    # ====== create attention if necessary ====== #

    # ====== calling rnn_warpper ====== #
    ## Bidirectional
    dynamic_flag = True
    bidirectional_flag = True
    # is_initialized_variables_flag = False
    rnn_func = None
    if bidirectional_flag == True:
        if dynamic_flag == True:
            rnn_func = tf.nn.bidirectional_dynamic_rnn
        else:
            # rnn_func = tf.nn.static_bidirectional_rnn
            rnn_func = tf.nn.bidirectional_dynamic_rnn
        state_fw, state_bw = None, None
        rnn_fw_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        # rnn_fw_cell = BNLSTMCell(RNN_CELL_SIZE, training)
        # rnn_fw_cell = GRU(
        #     RNN_CELL_SIZE, activation=tf.nn.relu, reuse=None,
        #     normalizer=tf.contrib.layers.layer_norm,
        #     initializer=tf.contrib.layers.xavier_initializer()
        # )
        rnn_bw_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        # rnn_bw_cell = BNLSTMCell(RNN_CELL_SIZE, training)
        # rnn_bw_cell = GRU(
        #     RNN_CELL_SIZE, activation=tf.nn.relu, reuse=None,
        #     normalizer=tf.contrib.layers.layer_norm,
        #     initializer=tf.contrib.layers.xavier_initializer()
        # )
        outputs = rnn_func(cell_fw=rnn_fw_cell, cell_bw=rnn_bw_cell, inputs=X_data_numerical,
                           initial_state_fw=state_fw,
                           initial_state_bw=state_bw,
                           dtype=X_data_numerical.dtype.base_dtype)
    ## Unidirectional
    else:
        cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        # cell = GRU(
        #     RNN_CELL_SIZE, activation=tf.nn.relu, reuse=None,
        #     normalizer=tf.contrib.layers.layer_norm,
        #     initializer=tf.contrib.layers.xavier_initializer()
        # )
        if dynamic_flag == True:
            rnn_func = tf.nn.dynamic_rnn
        else:
            rnn_func = tf.contrib.rnn.static_rnn
        outputs = rnn_func(cell, inputs=X_data_numerical, initial_state=state,
                           dtype=X_data_numerical.dtype.base_dtype)

    # ====== return ====== #
    if bidirectional_flag == True:  # concat outputs
        # outputs = (tf.concat(outputs[0], axis=-1), outputs[1])
        outputs_0 = batch_norm(outputs[0][0], outputs[0][0]._shape_as_list()[2], training, scope='Batch_Normalization')
        outputs_1 = batch_norm(outputs[0][1], outputs[0][1]._shape_as_list()[2], training, scope='Batch_Normalization')
        # outputs = tf.concat(outputs[0], axis=2)
        outputs = tf.concat((outputs_0, outputs_1), axis=2)
    else:
        return outputs[0]

    outputs = batch_norm(outputs, outputs.shape.as_list()[2], training, scope='Batch_Normalization')

    return outputs


def build_multi_dynamic_brnn_3(X_data_numerical, RNN_CELL_SIZE=128, sequence_length=16, dropout_enabled=True, dropout_keep_prob_value=0.8):
    bidirectional_flag = True
    if not bidirectional_flag:
        # with tf.variable_scope('Dynamic_RNN'):
        cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        if dropout_enabled:
            cell = tf.contrib.rnn.DropoutWrapper(cell=cell, output_keep_prob=dropout_keep_prob_value)

        outputs, _ = tf.nn.dynamic_rnn(cell=cell, inputs=X_data_numerical,
                                       sequence_length=sequence_length,
                                       swap_memory=True, dtype=tf.float32)
        final_outputs = outputs[:, -1, :]

        return final_outputs

    else:
        with tf.variable_scope('Bidirectional_RNN'):
            fw_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
            bw_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)

            if dropout_enabled:
                fw_cell = tf.contrib.rnn.DropoutWrapper(cell=fw_cell, output_keep_prob=dropout_keep_prob_value)
                bw_cell = tf.contrib.rnn.DropoutWrapper(cell=bw_cell, output_keep_prob=dropout_keep_prob_value)

            (encoder_outputs_fw, encoder_outputs_bw), _ = tf.nn.bidirectional_dynamic_rnn(
                cell_fw=fw_cell, cell_bw=bw_cell,
                inputs=X_data_numerical,
                # sequence_length=sequence_length,
                swap_memory=True,
                dtype=tf.float32)

            outputs = tf.concat([encoder_outputs_fw, encoder_outputs_bw], 2)

    return outputs
        #
        # final_outputs = outputs[:, -1, :]
        #
        # return final_outputs


def build_rnn_1(inputs, dropout_keep_prob_value, sequence_length, RNN_CELL_SIZE, reg=0.1):
    def unpack_sequence(tensor):
        return tf.unstack(tf.transpose(tensor, perm=[1, 0, 2]))

    with tf.variable_scope("Composition", initializer=tf.contrib.layers.xavier_initializer(),
                           regularizer=tf.contrib.layers.l2_regularizer(scale=reg)):
        cell = tf.contrib.rnn.LSTMCell(RNN_CELL_SIZE)
        # tf.cond(tf.less(self.dropout
        # if tf.less(self.dropout, tf.constant(1.0)):
        cell = tf.contrib.rnn.DropoutWrapper(cell,
                                             output_keep_prob=dropout_keep_prob_value, input_keep_prob=dropout_keep_prob_value)
        # output, state = tf.nn.dynamic_rnn(cell,emb,sequence_length=self.lngths,dtype=tf.float32)
        outputs, _ = tf.nn.dynamic_rnn(cell,
                                       # unpack_sequence(inputs),
                                       inputs,
                                       # sequence_length=sequence_length,
                                       dtype=tf.float32)
        # output = pack_sequence(outputs)

    # sum_out = tf.reduce_sum(tf.stack(outputs), [0])
    # sent_rep = tf.div(sum_out, tf.expand_dims(tf.to_float(sequence_length), 0))
    # final_outputs = sent_rep

    return outputs


def build_rnn_2(inputs, name):
    print("Building network " + name)
    # Define weights
    __n_hidden = 32
    n_output = (inputs.shape[0])._value
    weights = tf.Variable(tf.random_normal([__n_hidden, n_output]), name=name + "_weights")
    biases = tf.Variable(tf.random_normal([n_output]), name=name + "_biases")

    __cell_kind = tf.contrib.rnn.GRUCell
    # Define a lstm cell with tensorflow
    outputs, states = tf.nn.dynamic_rnn(
        __cell_kind(__n_hidden),
        inputs,
        # sequence_length=(inputs.shape[1])._value,
        dtype=tf.float32,
        scope=name,
        # time_major=False
    )
    __batch_size = (inputs.shape[0])._value
    __n_steps = (inputs.shape[1])._value
    assert outputs.get_shape() == (__batch_size, __n_steps, __n_hidden)
    print("Done building network " + name)
    # All these asserts are actually documentation: they can't be out of date
    outputs = tf.expand_dims(outputs, 2)
    assert outputs.get_shape() == (__batch_size, __n_steps, 1, __n_hidden)

    tiled_weights = tf.tile(tf.expand_dims(tf.expand_dims(weights, 0), 0), [__batch_size, __n_steps, 1, 1])
    assert tiled_weights.get_shape() == (__batch_size, __n_steps, __n_hidden, n_output)
    # assert tiled_weights.get_shape() == (1, 1, __n_hidden, n_output)
    # Linear activation, using rnn inner loop output for each char
    # finals = tf.batch_matmul(outputs, tiled_weights) + biases
    finals = tf.matmul(outputs, tiled_weights) + biases
    assert finals.get_shape() == (__batch_size, __n_steps, 1, n_output)
    return tf.squeeze(finals)


def build_rnn_3(inputs, name):
    print("Building network " + name)
    lens = (inputs.shape[1])._value
    __n_hidden = 32
    n_output = (inputs.shape[0])._value

    # Define weights
    # inputs = tf.gather(one_hots, inputs)
    weights = tf.Variable(tf.random_normal([__n_hidden, n_output]), name=name + "_weights")
    biases = tf.Variable(tf.random_normal([n_output]), name=name + "_biases")

    # Define a lstm cell with tensorflow
    __cell_kind = tf.contrib.rnn.GRUCell
    outputs, states = tf.nn.dynamic_rnn(
        __cell_kind(__n_hidden),
        inputs,
        # sequence_length=lens,
        dtype=tf.float32,
        scope=name,
        # time_major=False
    )

    # Prepare data shape to match `rnn` function requirements
    # Current data input shape: (__batch_size, __n_steps, n_input)
    # Required shape: '__n_steps' tensors list of shape (__batch_size, n_input)

    '''outputs, states = rnn.rnn(
        __cell_kind(__n_hidden),
        tf.unpack(tf.transpose(inputs, [1, 0, 2])),
        sequence_length=lens,
        dtype=tf.float32,
        scope=name)
    outputs = tf.transpose(tf.pack(outputs), [1, 0, 2])'''

    print("Done building network " + name)

    __batch_size = (inputs.shape[0])._value
    __n_steps = (inputs.shape[1])._value
    # Asserts are actually documentation: they can't be out of date
    assert outputs.get_shape() == (__batch_size, __n_steps, __n_hidden)
    # Linear activation, using rnn output for each char
    # Reshaping here for a `batch` matrix multiply
    # It's faster than `batch_matmul` probably because it can guarantee a
    # static shape
    outputs = tf.reshape(outputs, [__batch_size * __n_steps, __n_hidden])
    finals = tf.matmul(outputs, weights)
    return tf.reshape(finals, [__batch_size, __n_steps, n_output]) + biases


def build_rnn_4(inputs, RNN_CELL_SIZE, dropout=0.8, reg=0.1):
    lngths = (inputs.shape[1])._value

    def unpack_sequence(tensor):
        return tf.unstack(tf.transpose(tensor, perm=[1, 0, 2]))

    with tf.variable_scope("Composition", initializer=tf.contrib.layers.xavier_initializer(),
                           regularizer=tf.contrib.layers.l2_regularizer(reg)):
        cell_fw = tf.contrib.rnn.LSTMCell(RNN_CELL_SIZE)
        cell_bw = tf.contrib.rnn.LSTMCell(RNN_CELL_SIZE)
        # tf.cond(tf.less(self.dropout
        # if tf.less(self.dropout, tf.constant(1.0)):
        cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw,
                                                output_keep_prob=dropout, input_keep_prob=dropout)
        cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, output_keep_prob=dropout, input_keep_prob=dropout)

        # output, state = rnn.dynamic_rnn(cell,emb,sequence_length=self.lngths,dtype=tf.float32)
        outputs, _ = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw,
                                                     # unpack_sequence(inputs),
                                                     inputs,
                                                     # sequence_length=lngths,
                                                     dtype=tf.float32
                                                     )
        # output = pack_sequence(outputs)
        outputs = tf.concat(outputs, axis=2)
    # sum_out = tf.reduce_sum(tf.stack(outputs), [0])
    # sent_rep = tf.div(sum_out, tf.expand_dims(tf.to_float(lngths), 1))
    # final_state = sent_rep
    # return final_state

    return outputs


# def ndlstm_base_dynamic(inputs, noutput, scope=None, reverse=False):
def build_rnn_5(inputs, RNN_CELL_SIZE, scope=None, reverse=False):
    noutput = RNN_CELL_SIZE
    """Run an LSTM, either forward or backward.

  This is a 1D LSTM implementation using dynamic_rnn and
  the TensorFlow LSTM op.

  Args:
    inputs: input sequence (length, batch_size, ninput)
    noutput: depth of output
    scope: optional scope name
    reverse: run LSTM in reverse

  Returns:
    Output sequence (length, batch_size, noutput)
  """
    with tf.variable_scope(scope, "SeqLstm", [inputs]):
        # TODO(tmb) make batch size, sequence_length dynamic
        # example: sequence_length = tf.shape(inputs)[0]
        batch_size = (inputs.shape[0])._value
        # lstm_cell = tf.contrib.rnn.BasicLSTMCell(noutput, state_is_tuple=False)
        lstm_cell = tf.contrib.rnn.GRUCell(noutput)
        # state = tf.zeros([batch_size, lstm_cell.state_size])
        # state = tf.zeros([batch_size, lstm_cell.state_size[0]*2])
        # state = tf.zeros([batch_size, int(batch_size / 2)])
        sequence_length = int(inputs.get_shape()[0])
        sequence_lengths = tf.to_int64(tf.fill([batch_size], sequence_length))
        if reverse:
            inputs = tf.reverse_v2(inputs, [0])
        # outputs, _ = tf.nn.dynamic_rnn(lstm_cell, inputs, sequence_lengths, state, time_major=True)
        # outputs, _ = tf.nn.dynamic_rnn(lstm_cell, inputs, sequence_lengths, state)
        # outputs, _ = tf.nn.dynamic_rnn(cell=lstm_cell, inputs=inputs, initial_state=state)
        outputs, _ = tf.nn.dynamic_rnn(cell=lstm_cell, inputs=inputs, dtype=inputs.dtype)
        if reverse:
            outputs = tf.reverse_v2(outputs, [0])

        return outputs


def build_rnn_6(concat_inputs, cell_type, RNN_CELL_SIZE, num_layer, dropout_keep_prob_value):

    with tf.variable_scope('Encoder'):

        def get_single_cell(cell_type):
            if cell_type == "GRU":
                single_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
            elif cell_type == "LSTM":
                single_cell = tf.contrib.rnn.BasicLSTMCell(RNN_CELL_SIZE, forget_bias=1.0)

            with tf.name_scope('Dropout_Wrapper'):
                single_cell = tf.contrib.rnn.DropoutWrapper(
                    single_cell,
                    input_keep_prob=dropout_keep_prob_value,
                    output_keep_prob=dropout_keep_prob_value,
                    # state_keep_prob=dropout_keep_prob_value,
                    variational_recurrent=False,
                    input_size=None,
                    dtype=None,
                    seed=None,
                    dropout_state_filter_visitor=None
                )

            return single_cell

        fw_cells = []
        bw_cells = []

        for i in range(0, num_layer):
            fw_cells.append(get_single_cell(cell_type))
            bw_cells.append(get_single_cell(cell_type))

        (outputs, output_state_fw, output_state_bw) = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(
            cells_bw=bw_cells,
            cells_fw=fw_cells,
            # inputs=numeric_inputs,
            inputs=concat_inputs,
            dtype=tf.float32)

        if cell_type == "LSTM":
            # The states of lstm and the remaining cells are different. LSTM is a tuple type
            encoder_final_state_c = tf.concat((output_state_fw[-1].c, output_state_bw[-1].c), 1)
            encoder_final_state_h = tf.concat((output_state_fw[-1].h, output_state_bw[-1].h), 1)
            encoder_final_state = tf.contrib.rnn.LSTMStateTuple(c=encoder_final_state_c, h=encoder_final_state_h)
        else:
            encoder_final_state = tf.concat((output_state_fw[-1], output_state_bw[-1]), 1)

    return outputs, encoder_final_state


def build_rnn_7(concat_inputs, RNN_CELL_SIZE, HIDDEN_LAYER_SIZE, SEQUENCE_LENGTH):
    # with tf.name_scope('Bidirectional_Dynamic_RNN'):
    # Add LSTM layers
    rnn_fw_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
    rnn_bw_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
    outputs, states = tf.nn.bidirectional_dynamic_rnn(
        cell_fw=rnn_fw_cell,
        cell_bw=rnn_bw_cell,
        dtype=tf.float32,
        inputs=concat_inputs)

    print('outputs[0].shape before concat: ', outputs[0].shape)
    outputs = tf.concat(outputs, axis=2)
    print('outputs.shape after concat: ', outputs.shape)

    # with tf.name_scope('Attention'):
    encoding, alphas = my_attention(outputs, HIDDEN_LAYER_SIZE, SEQUENCE_LENGTH)

    return encoding


def build_rnn_8(concat_inputs, RNN_CELL_SIZE, num_layer, cell_type):
    def get_single_cell(cell_type):
        if cell_type == "GRU":
            single_cell = tf.contrib.rnn.GRUCell(RNN_CELL_SIZE)
        elif cell_type == "LSTM":
            single_cell = tf.contrib.rnn.BasicLSTMCell(RNN_CELL_SIZE, forget_bias=1.0)
        return single_cell

    with tf.variable_scope('Encoder'):

        fw_cells = []
        bw_cells = []

        for i in range(0, num_layer):
            fw_cells.append(get_single_cell(cell_type))
            bw_cells.append(get_single_cell(cell_type))

        (outputs, output_state_fw, output_state_bw) = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(
            cells_bw=bw_cells,
            cells_fw=fw_cells,
            inputs=concat_inputs,
            dtype=tf.float32)

        if cell_type == "LSTM":
            # The states of lstm and the remaining cells are different. LSTM is a tuple type
            encoder_final_state_c = tf.concat((output_state_fw[-1].c, output_state_bw[-1].c), 1)
            encoder_final_state_h = tf.concat((output_state_fw[-1].h, output_state_bw[-1].h), 1)
            encoder_final_state = tf.contrib.rnn.LSTMStateTuple(c=encoder_final_state_c, h=encoder_final_state_h)
        else:
            encoder_final_state = tf.concat((output_state_fw[-1], output_state_bw[-1]), 1)

    with tf.variable_scope('Decoder'):
        # decoder_cell = tf.contrib.rnn.LSTMCell(num_units=RNN_CELL_SIZE * 2, state_is_tuple=True)
        # decoder_cell = tf.contrib.rnn.LSTMCell(num_units=RNN_CELL_SIZE * 2, state_is_tuple=True)
        _cell = tf.contrib.rnn.GRUCell(num_units=RNN_CELL_SIZE * 2)

        decoder_outputs, decoder_final_state = tf.nn.dynamic_rnn(
            cell=_cell, inputs=outputs,
            initial_state=encoder_final_state,
            dtype=tf.float32)

    return decoder_outputs


def batch_norm(x, n_out, phase_train, scope='Batch_Normalization'):
    #http://stackoverflow.com/questions/33949786/how-could-i-use-batch-normalization-in-tensorflow
    """
    Batch normalization on convolutional maps.
    Args:
        x:           Tensor, 4D BHWD input maps
        n_out:       integer, depth of input maps
        phase_train: boolean tf.Varialbe, true indicates training phase
        scope:       string, variable scope
    Return:
        normed:      batch-normalized maps
    """
    with tf.variable_scope(scope):
        beta = tf.Variable(tf.constant(0.0, shape=[n_out]),
                                     name='beta', trainable=True)
        gamma = tf.Variable(tf.constant(1.0, shape=[n_out]),
                                      name='gamma', trainable=True)
        batch_mean, batch_var = tf.nn.moments(x, [0,1,2], name='moments')
        ema = tf.train.ExponentialMovingAverage(decay=0.5)

        def mean_var_with_update():
            ema_apply_op = ema.apply([batch_mean, batch_var])
            with tf.control_dependencies([ema_apply_op]):
                return tf.identity(batch_mean), tf.identity(batch_var)

        mean, var = tf.cond(phase_train,
                            mean_var_with_update,
                            lambda: (ema.average(batch_mean), ema.average(batch_var)))
        normed = tf.nn.batch_normalization(x, mean, var, beta, gamma, 1e-3)
    return normed

