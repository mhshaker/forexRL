
import numpy as np
import math
import tensorflow as tf
from tensorflow.contrib import rnn
import tf_util as util
import os, os.path, time, math, random
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
random.seed(1)
np.random.seed(1)
tf.set_random_seed(1)
np.set_printoptions(threshold=np.nan)
n_nodes = 50

class DeepQNetwork:
    def __init__(
            self,
            n_actions,
            n_features,
            seq_len,
            learning_rate=0.01,
            reward_decay=0.99,
            e_greedy_init=0.1,
            e_greedy_min=0.1,
            e_greedy_decrement=0.000001,

            dropout=0.9,
            writer_name=""
    ):
        self.dropout = dropout
        self.step = 0
        self.gamma = reward_decay
        self.learning_rate = learning_rate
        self.epsilon_min = e_greedy_min
        self.epsilon_decrement = e_greedy_decrement
        self.epsilon = e_greedy_init #if e_greedy_decrement is not None else self.epsilon_min
        self.n_actions = n_actions
        tf.reset_default_graph()

        with tf.name_scope('inputs'):
            self.observation = tf.placeholder(tf.float32, [None, seq_len, n_features], name='observation')
            self.rewards = tf.placeholder(tf.float32, [None, 1], name='reward')  
            self.prewards = tf.placeholder(tf.float32, [None, 1], name='pip_reward')  
            self.actions = tf.placeholder(tf.int32, [None,], name='action') 
            self.pkeep = tf.placeholder(tf.float32, name='dropout')  
            # self.lr = tf.placeholder(tf.float32, name='lr')
            # self.global_step = tf.placeholder(tf.float32, name='step')

        with tf.variable_scope('model'):

            # l = util.convLayer(self.observation, 32, 6, "conv1")
            # l = util.squeeze_layer(l,16,32,"squeeze1")
            # l = tf.layers.max_pooling1d(l,2,2,name="maxpool1")
            # l = util.squeeze_layer(l,16,64,"squeeze2")
            # l = util.squeeze_layer(l,32,64,"squeeze3")
            # l = tf.layers.max_pooling1d(l,2,2,name="maxpool2")
            # l = util.squeeze_layer(l,32,64,"squeeze4")
            # l = util.squeeze_layer(l,16,32,"squeeze5")
            # l = tf.layers.max_pooling1d(l,2,2,name="maxpool3")
            
            l = util.convLayer(self.observation, 5, 3, "conv1")
            l = util.convLayer(l, 10, 3, "conv2")
            l = util.convLayer(l, 20, 3, "conv3")
            l = util.convLayer(l, 30, 1, "conv4")
            l = util.convLayer(l, 40, 1, "conv5")


            self.logits = tf.layers.dense(l[:, -1], #[batch, feature]
                n_actions, 
                activation=None, 
                bias_initializer=tf.constant_initializer(0.1),
                kernel_initializer=tf.contrib.layers.xavier_initializer(seed=1), 
                name='logits') # l[:, -1]

            # self.action = tf.nn.softmax(self.logits)
            # self.sample_op = tf.multinomial(logits=self.logits,
            #                                 num_samples=1, 
            #                                 seed= 1, 
            #                                 name="sample_op")

        # with tf.variable_scope('train'):
        #     neg_log_prob = tf.nn.softmax_cross_entropy_with_logits(logits=self.logits, 
        #         labels=tf.one_hot(self.actions,n_actions))
        #     self.loss = tf.reduce_mean(self.rewards * neg_log_prob + 1E-4) * 1000             
        #     update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        #     with tf.control_dependencies(update_ops):
        #         self.train_op = tf.train.AdamOptimizer(learning_rate).minimize(self.loss)
        #     loss_summary = tf.summary.scalar("loss", self.loss)

        with tf.variable_scope('q_target'):
            q_target = self.rewards + self.gamma * tf.reduce_max(self.logits, axis=1, name='Qmax_s_')    # shape=(None, )
            # self.q_target = tf.stop_gradient(q_target)
        
        with tf.variable_scope('q_eval'):
            a_indices = tf.stack([tf.range(tf.shape(self.actions)[0], dtype=tf.int32), self.actions], axis=1)
            self.q_eval_wrt_a = tf.gather_nd(params=self.logits, indices=a_indices)    # shape=(None, )
        
        with tf.variable_scope('cost'):
            self.loss = tf.reduce_mean(tf.squared_difference(q_target, self.q_eval_wrt_a, name='cost')) * 100
        
        with tf.variable_scope('train'):
            self.train_op = tf.train.RMSPropOptimizer(self.learning_rate).minimize(self.loss)
            loss_summary = tf.summary.scalar("batch_loss", self.loss)

        with tf.name_scope('performance_main'):
            reward_summary = tf.summary.scalar("pip_rewards", tf.reduce_sum(self.prewards))
            reward_summary = tf.summary.scalar("Qvalues", tf.reduce_mean(self.q_eval_wrt_a))
        
        with tf.variable_scope('util'):
            for var in tf.trainable_variables():
                print("var.name: ",var.name)
                tf.summary.histogram(var.name, var)


        self.summaries = tf.summary.merge_all()

        self.sess = tf.InteractiveSession()

        self.summary_writer = tf.summary.FileWriter("logs/" + writer_name, self.sess.graph)
        self.validation_writer = tf.summary.FileWriter("logs/" + writer_name + "_valid", self.sess.graph)
        if not os.path.exists("checkpoints"):
            os.mkdir("checkpoints")
        self.saver = tf.train.Saver(max_to_keep=1000)

        tf.global_variables_initializer().run()

    def learn_p(self, o, a, r, batch_size=0, online=False):
        # replay memmory
        if batch_size != 0:
            idx = np.random.choice(np.arange(len(a)), batch_size, replace=False)
            o = o[idx]
            a = a[idx]
            r = r[idx]

        # discounted_r = self.discount_rewards(r)
        rewards = np.array(r).reshape(-1,1)
        pip_rewards = np.array(r).reshape(-1,1)

        feed_dict = feed_dict={
            self.observation: o,
            self.actions: a,
            self.rewards: rewards,
            self.prewards: pip_rewards,
            self.pkeep: self.dropout,
            # self.lr: self.learning_rate,
            # self.global_step: self.step
        }

        _, smm,l = self.sess.run([self.train_op, self.summaries, self.logits], feed_dict=feed_dict)
        if self.step % 10 == 0 and not online:
            self.summary_writer.add_summary(smm, self.step)
        if online == True:
            self.validation_writer.add_summary(smm, self.step)
        
        # increasing epsilon
        self.epsilon = self.epsilon - self.epsilon_decrement if self.epsilon > self.epsilon_min else self.epsilon_min

        self.step += 1

    
    def valid_p(self, o, a, r):
        # discounted_r = self.discount_rewards(r)
        rewards = np.array(r).reshape(-1,1)
        pip_rewards = np.array(r).reshape(-1,1)

        smm = self.sess.run(
            self.summaries,
            feed_dict={
                self.observation: o,
                self.actions: a,
                self.rewards: rewards, # self.memory[:, self.n_features + 1],
                self.prewards: pip_rewards,
                self.pkeep: 1,
                # self.global_step: self.step
            })
        self.validation_writer.add_summary(smm, self.step)
   
    def discount_rewards(self, rewards, gamma=0.99):
        discounted_r = np.zeros_like(rewards,dtype=np.float32)
        counter = 1
        for t in reversed(range(0, len(rewards))):
            if rewards[t] != 0:
                discounted_r[t] = rewards[t]
                counter = 1
            else:
                # print("reseting discount")
                if t+1 < len(rewards):
                    discounted_r[t] = discounted_r[t+1] * (gamma)
                else:
                    discounted_r[t] = rewards[t]
                counter += 1

        return discounted_r
    
    def choose_action_one(self, observation):
        observation = observation[np.newaxis, :]
        action, logits = self.sess.run([self.sample_op, self.logits], feed_dict={self.observation: observation, self.pkeep: 1})
        return action[0][0]
    
    def choose_action_batch(self, observation):
        if np.random.uniform() > self.epsilon:
            # forward feed the observation and get q value for every actions
            actions_value = self.sess.run(self.logits, feed_dict={self.observation: observation, self.pkeep: 1})
            # print("predictions value ",actions_value)
            action = np.argmax(actions_value,axis=1)
            print("predictions actions ",action)
        else:
            action = np.random.randint(0, self.n_actions, size=len(observation))
            # print("random actions ",action)

        return action
    
    # def choose_action_batch(self, observation):
    #     action, logits = self.sess.run([self.sample_op, self.logits], feed_dict={self.observation: observation, self.pkeep: 1})
    #     action = np.reshape(action, -1)
    #     return action