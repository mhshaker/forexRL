import numpy as np
import time
from numba import vectorize
from numba import guvectorize
import numba as nb
# def add(a,b,c):
# 	for x in range(a.size):
# 		c[x] = a[x] + b[x]
# @vectorize(["float32, float32(float32,float32)"], target='cuda')
# @nb.jit("float32(float32,float32)",nopython=True)
i = 0
def function(a):
	global i
	i += 1
	return a * i

@nb.jit
def add(a,b):
	# print(a)
	return a + b + function(a) #, a - b
# @vectorize(["Tuple((float32,float32))(float32,float32)"])
@nb.jit('Tuple((float32[:,:,:], float32[:,:,:]))(float32[:,:,:], float32[:,:,:])',nopython=True)
def add2(a,b):
	return a + b , a - b

def add_(a,b,c):
	for x in range(a.size):
		c[x] = a[x] + b[x]
N = 92000000

A = np.ones(N, dtype=np.float32)
B = np.ones(N, dtype=np.float32)
C = np.zeros(N, dtype=np.float32)
D = np.ones(N, dtype=np.float32)


start = time.time()
for x in range(1,100):
	C = A + B
	# C = add(A,B)
	# add_(A,B,C)

print(">",str(C[:5]))
print(">",str(C[-5:]))
print(">",str(D[:5]))
print(">",str(D[-5:]))
print("time",time.time()-start)