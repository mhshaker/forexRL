
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
import time
import os, os.path
import tf_util as util
np.random.seed(1)


s_index = 0
e_index = 100
et_index = 100
spread = 0.2
initCapital = 1000
balance = initCapital
stoploss = -15
takeprofit = 30
last_action = 0
action_space = [1,-1] # 'buy', 'sell', 'non'
n_actions = len(action_space)

punish_Counter = 0

logActions_flag=False
balanceList = []
orderList = []
picCount = 0
last_reward = 0
pic_cat = "train"
picLimit_train = -3
picLimit_test = -3

if not os.path.exists("pics"):
    os.mkdir("pics")
if not os.path.exists("orderLogs"):
    os.mkdir("orderLogs")

# if not os.path.exists("pics/train"):
#     os.mkdir("pics/train")
# if not os.path.exists("pics/test"):
#     os.mkdir("pics/test")

# if not os.path.exists("orderLogs/train"):
#     os.mkdir("orderLogs/train")
# if not os.path.exists("orderLogs/test"):
#     os.mkdir("orderLogs/test")

def reset():
    global logActions_flag,balanceList,orderList,picCount,last_reward,pic_cat,picLimit_train,picLimit_test

    logActions_flag=False
    balanceList = []
    orderList = []
    picCount = 0
    last_reward = 0
    pic_cat = "train"
    picLimit_train = -3
    picLimit_test = -3


def read_data(datapath, features, start_index, end_index, end_test_index,seq_len=30,pic_name="run1", outliers=False):
    global data, s_index, e_index, et_index, index, n_features, target, obs_array, target_array, obs_array_t, target_array_t
    data = pd.read_csv(datapath)

    ##################

    data['icrossSell'] = list(map(ichi_cross_signal_sell,
        data.Tenkan_sen, data.Kijun_sen, 
        data.Tenkan_sen.shift(1), data.Kijun_sen.shift(1)))

    data['icrossBuy'] = list(map(ichi_cross_signal_buy,
        data.Tenkan_sen, data.Kijun_sen, 
        data.Tenkan_sen.shift(1), data.Kijun_sen.shift(1)))

    # data = data[400000:410000]
    # plt.plot(data.Tenkan_sen)
    # plt.plot(data.Kijun_sen)
    # plt.scatter(data.index, data.icrossBuy)
    # plt.show()
    # exit()

    ##################

    # features.append("open")
    # print(features)
    data = data[features]
    n_features = len(features)
    # data = add_features(data)
    data.dropna(inplace=True)
    s_index = start_index
    e_index = end_index
    et_index = end_test_index
    index = s_index

    if not os.path.exists("pics/"+pic_name):
        os.mkdir("pics/"+pic_name)
    # if not os.path.exists("pics/"+pic_name+"/test"):
    #     os.mkdir("pics/"+pic_name+"/test")
    # if not os.path.exists("pics/"+pic_name+"/train"):
    #     os.mkdir("pics/"+pic_name+"/train")


    plot_data(pic_name,True) # True
    # exit()
    # pData = data[e_index-seq_len:e_index+100]
    # a, = plt.plot(pData.close, label="close")
    # b, = plt.plot(pData.Tenkan_sen, label="T")
    # c, = plt.plot(pData.Kijun_sen, label="K")
    # d, = plt.plot(pData.Chikou_Span, label="C")
    # e, = plt.plot(pData.Span_A, label="A")
    # f, = plt.plot(pData.Span_B, label="B")
    # plt.legend(handles=[a, b,c,d,e,f])
    # plt.show()
    # exit()

    # data preprocessing
    data = convert_to_return(data) * 1000



    if outliers == True:
        sample_data = data[s_index-100:e_index+1]
        # plt.plot(sample_data.close)
        data_abs = abs(sample_data)
        # exit()
        for x in features:
            mean = data_abs[x].mean()
            std_threshold = data_abs[x].std() * 3

            sample_data[x] = sample_data[x].mask(sample_data[x] > std_threshold, mean)
            sample_data[x] = sample_data[x].mask(sample_data[x] < -1*std_threshold, -1*mean)

        data[s_index-100:e_index+1] = sample_data
        # plt.plot(sample_data.close)
        # plt.show()
    target = data.close * 10


    obs_array = [] 
    for x in range(s_index,e_index):
        obs_array.append(np.array(data[x-seq_len+1:x+1]))
        # print(obs_array)
        # exit()
    obs_array = np.array(obs_array)
    target_array = np.array(target[s_index+1:e_index+1])

    obs_array_t = []
    for x in range(e_index,et_index):
        obs_array_t.append(np.array(data[x-seq_len+1:x+1]))
    obs_array_t = np.array(obs_array_t)
    target_array_t = np.array(target[e_index+1:et_index+1])
    
    # print(obs_array.shape)
    # print(target_array.shape)

    # print(data[e_index-seq_len:e_index+100].head(10))

    # print(obs_array_t[1])
    # print(target_array_t[1])
    # exit()

    return data

def convert_to_return(data):
    # data.open = data.open.diff(1)
    # data.high = data.high.diff(1)
    # data.low = data.low.diff(1)
    data.close = data.close.diff(1)

    # data.volume = data.volume.diff(1)

    # data.MA5 = data.MA5.diff(1)
    # data.MA20 = data.MA20.diff(1)
    # data.MA50 = data.MA50.diff(1)

    data.xOpen = data.xOpen.diff(1)
    data.xHigh = data.xHigh.diff(1)
    data.xLow = data.xLow.diff(1)
    data.xClose = data.xClose.diff(1)

    data.Tenkan_sen = data.Tenkan_sen.diff(1)
    data.Kijun_sen = data.Kijun_sen.diff(1)
    data.Span_A = data.Span_A.diff(1)
    data.Span_B = data.Span_B.diff(1)
    # data.Chikou_Span = data.Chikou_Span.diff(1)

    data = data.round(5)
    data.dropna(inplace=True)
    return data.reset_index(drop=True)

def minMaxScaler(observation):
    numerator = observation - np.min(observation, 0)
    denominator = np.max(observation, 0) - np.min(observation, 0)
    return numerator / (denominator + 1e-7)

def normal(observation):
    return (observation - observation.mean()) / observation.std()

def ichi_cross_signal_buy(Tenkan_sen,Kijun_sen,Tenkan_sen_past,Kijun_sen_past):
    if Tenkan_sen > Kijun_sen and Tenkan_sen_past < Kijun_sen_past:
        return 1
    else:
        return 0

def ichi_cross_signal_sell(Tenkan_sen,Kijun_sen,Tenkan_sen_past,Kijun_sen_past):
    if Tenkan_sen < Kijun_sen and Tenkan_sen_past > Kijun_sen_past:
        return 1
    else:
        return 0

def ichi_komo_signal_buy(close,close_past,Span_B,Span_A):
    maxSpan = Span_A
    if Span_B > Span_A:
        maxSpan = Span_B
    if close > maxSpan and close_past < maxSpan:
        return 1
    else:
        return 0

def ichi_komo_signal_buy(close,close_past,Span_B,Span_A):
    minSpan = Span_A
    if Span_B < Span_A:
        minSpan = Span_B
    if close < minSpan and close_past > minSpan:
        return 1
    else:
        return 0

def plot_data(pic_name, merge=False):
    # print(data.head())
    if merge:
        plt.figure(figsize=(14,6.5))
        plt.plot(data.close[s_index:e_index])
        plt.xlabel("Time Period")
        plt.ylabel("Price")
        plt.plot(data.close[e_index:et_index])
        plt.savefig('pics/'+pic_name+'/Data.png')
        plt.close()
        # exit()
    else:
        plt.plot(data.close[s_index:e_index])
        plt.xlabel("Time Period")
        plt.ylabel("Price")
        plt.savefig('pics/'+pic_name+'/_trainData.png')
        # plt.show()
        plt.close()

        # print(data.head())
        plt.plot(data.close[e_index:et_index])
        plt.xlabel("Time Period")
        plt.ylabel("Price")
        plt.savefig('pics/'+pic_name+'/_testData.png')
        # plt.show()
        plt.close()

def plot_balance():
    global picCount
    picCount+=1
    plt.plot(balanceList)
    plt.xlabel("Time Period")
    plt.ylabel("Profit")
    plt.savefig('pics/'+pic_cat+"/"+str(picCount)+'.png')
    plt.close()
    with open("orderLogs/"+pic_cat+"/"+str(picCount)+"_orderLog.txt", "w", encoding='utf-8') as text_file:
        mytext = '\n'.join(map(str, orderList))
        text_file.write(mytext)

def plot_balance_p(rewards, epoch, pic_cat="train", pic_name="run1",rand_rewards=[], bh_rewsrds=[], sh_rewsrds=[]):
    global picCount, picLimit_train, picLimit_test
    profit = np.sum(rewards)
    if pic_cat == "train":
        picLimit = picLimit_train
    else:
        picLimit = picLimit_test
    if profit > 100 * picLimit or pic_cat=="test":
        picCount+=1
        balanceList = []
        balance = 0
        for x in range(len(rewards)):
            balance = balance + rewards[x]
            balanceList.append(balance)
        a, = plt.plot(balanceList,label="DeepRL")
        if len(rand_rewards) > 1:
            # print("ploting Random Walk")
            balanceList = []
            balance = 0
            for x in range(len(rand_rewards)):
                balance = balance + rand_rewards[x]
                balanceList.append(balance)
            b, = plt.plot(balanceList,label="Random Walk")
            # plt.legend(handles=[a, b])

        if len(bh_rewsrds) > 1:
            # print("ploting BuyAndHold")
            balanceList = []
            balance = 0
            for x in range(len(bh_rewsrds)):
                balance = balance + bh_rewsrds[x]
                balanceList.append(balance)
            c, = plt.plot(balanceList,label="Buy and Hold")
            # plt.legend(handles=[a, b,c])

        if len(sh_rewsrds) > 1:
            # print("ploting BuyAndHold")
            balanceList = []
            balance = 0
            for x in range(len(sh_rewsrds)):
                balance = balance + sh_rewsrds[x]
                balanceList.append(balance)
            d, = plt.plot(balanceList,label="Sell and Hold")
            plt.legend(handles=[a, b,c,d])

        plt.xlabel("Time Period")
        plt.ylabel("Profit")
        plt.savefig('pics/'+pic_name+"/"+str(picCount)+'_'+str(epoch)+'.png')
        plt.close()
        with open("orderLogs/"+pic_cat+"/"+str(picCount)+"_orderLog.txt", "w", encoding='utf-8') as text_file:
            mytext = '\n'.join(map(str, rewards))
            text_file.write(mytext)
        if pic_cat == "train":
            picLimit_train += 1
        else:
            picLimit_test += 1

def plot_res(a,p=0.8):
    a = util.getData(a,p)
    return a

def logActions(action, pipReward):
    global balance, balanceList, orderList
    balance += pipReward
    balanceList.append(balance)
    orderList.append([action,round(pipReward,0)])

def next_batch(batch_size, nb_epochs, log=False):

    data_len = obs_array.shape[0]
    nb_batches = (data_len - 1) // (batch_size)
    assert nb_batches > 0, "Not enough data, even for a single batch. Try using a smaller batch_size."
    rounded_data_len = nb_batches * batch_size 

    xdata = obs_array[0:rounded_data_len]
    tpdata = target_array[0:rounded_data_len]

    # print("nb_batches: ",nb_batches)
    # print("rounded_data_len: ",rounded_data_len)


    for epoch in range(nb_epochs):
        for batch in range(nb_batches):
            x = xdata[batch * batch_size :(batch + 1) * batch_size]
            p = tpdata[batch * batch_size :(batch + 1) * batch_size]

            if log == True:
                couter = 0
                print("┌───────────────────────────────────────────────────────────────────────────────────────────────────────────┐e",epoch+1,"b",batch+1)
                for i in range(0,len(x)):
                    couter +=1
                    print("..........................................................................",couter)
                    print(x[i], "->", "(",p[i],")")
                    # print(y[i])
                print("└───────────────────────────────────────────────────────────────────────────────────────────────────────────┘")

            yield x, p, epoch, batch  


def next_batch_test(batch_size, log=False):

    data_len = obs_array_t.shape[0]
    nb_batches = (data_len - 1) // (batch_size)
    assert nb_batches > 0, "Not enough data, even for a single batch. Try using a smaller batch_size."
    rounded_data_len = nb_batches * batch_size 

    xdata = obs_array_t[0:rounded_data_len]
    tpdata = target_array_t[0:rounded_data_len]


    for batch in range(nb_batches):
        x = xdata[batch * batch_size :(batch + 1) * batch_size]
        p = tpdata[batch * batch_size :(batch + 1) * batch_size]

        if log == True:
            couter = 0
            print("┌───────────────────────────────────────────────────────────────────────────────────────────────────────────┐e",epoch+1,"b",batch+1)
            for i in range(0,len(x)):
                couter +=1
                print("..........................................................................",couter)
                print(x[i], "->", y[i], "(",p[i],")")
                # print(y[i])
            print("└───────────────────────────────────────────────────────────────────────────────────────────────────────────┘")

        yield x, p, batch  