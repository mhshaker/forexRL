import numpy as np
import forex_env as env
from RL_brain import PolicyGradient
from DQN_brain import DeepQNetwork
import time
import random
# from tqdm import tqdm
random.seed(1)
np.random.seed(1)
rand_state = random.getstate()

istart   = 349000 # 450000 #0400001  #55001 #'2001.01.02'
iend     = 351000 #'2001.02.05'
iendTest = 352000 #'2001.01.30'
datapath = '../Data/EURUSD15_pRL.csv'
seq_len = 5
batch_size = 32
epochs = 500
dropout = 1
n_layres = 2
valid_falg = False
outlier_flag = True


def make_hparam_string(learning_rate):
    return "_lr_%.0E" % (learning_rate)

def convert_action(a):
    return env.action_space[a]

def convert_target_to_label(t):
    if t > 0:
        return 0
    else:
        return 1

def get_spread(action, last_action):
    s = 0
    if (last_action == 1 and action == -1) or (last_action == -1 and action == 1) or (last_action == 0 and action != last_action):
        s = env.spread
    return s
  
all_sum_reward = []
good_run_index = []
for x in range(60,70): #[2,7,16,50,60,65,73,78,86,94]: #range(50,100):
    # x = 50 + x
    random.setstate(rand_state)
    print("run ",x)
    start = istart + x * 1000
    end = iend + x * 1000
    endTest = iendTest + x * 1000
    env.reset()
    # print("Run name: dense2_seq5_epoch500_outlierOn_layrer1")
    run_name = "search_dense"+ str(n_layres) + "_batch"+str(batch_size)+"_seq"+str(seq_len)+"["+ str(start) +"_"+str(end)+"_"+ str(endTest)+"]"

    df = env.read_data(datapath,
                        [
                         # 'open',
                         # 'high',
                         # 'low',
                         'Tenkan_sen',
                         'Kijun_sen',
                         'Span_A',
                         'Span_B',
                         # 'Chikou_Span',
                         # 'icrossSell',
                         # 'icrossBuy',
                         'xOpen',
                         'xHigh',
                         'xLow',
                         'xClose',
                         'close'
                         ],
                         start, 
                         end, 
                         endTest, 
                         seq_len=seq_len, 
                         pic_name=run_name,
                         outliers=outlier_flag) # ,'volume'

    max_reward = (abs(env.target_array_t)).sum()
    print("max_reward:",max_reward) 
    # exit()

    for learning_rate in [5E-4]: #  , 1E-3, 1E-4,1E-5, 1E-10
        hparam = make_hparam_string(learning_rate)

        RL = PolicyGradient(n_actions=env.n_actions, 
            n_features=env.n_features,
            seq_len= seq_len,
            learning_rate=learning_rate, 
            dropout= dropout,
            writer_name=run_name + hparam,
            run_model= n_layres)

        # allrewards = []
        for x_, t_, epoch, batchNo in env.next_batch(batch_size=batch_size, nb_epochs=epochs): #2000 # 500
            
            # validation
            if epoch % 2 == 0 and valid_falg == True:
                at = RL.choose_action_batch_noSample_op(env.obs_array_t)
                # old version with the better results (the on in the thesis)
                # at = RL.choose_action_batch(env.obs_array_t)
                act = list(map(convert_action , at))
                st = list(map(get_spread,act,np.roll(act, -1)))
                # p = list(np.roll(at, -1))
                l = list(map(convert_target_to_label , env.target_array_t))
                rt = act * env.target_array_t
                rt = rt - st
                RL.valid_p(env.obs_array_t,at,rt,l)

            # learn
            a = RL.choose_action_batch_noSample_op(x_)
            ac = list(map(convert_action , a))
            s = list(map(get_spread,ac,np.roll(ac, -1)))
            # p = list(np.roll(a, -1))
            l = list(map(convert_target_to_label , t_))
            r = ac * t_
            r = r - s
            RL.learn_p(x_,a,r,l)

        # final train and the performance picture
        at = RL.choose_action_batch_noSample_op(env.obs_array)
        # print(np.array(at).shape)
        act = list(map(convert_action , at))
        # print(np.array(act).shape)
        st = list(map(get_spread,act,np.roll(act, -1)))
        # print(np.array(st).shape)
        rt = act * env.target_array
        total_train_rewards = rt - st


        # final test and the performance picture
        at = RL.choose_action_batch_noSample_op(env.obs_array_t)
        act = list(map(convert_action , at))
        st = list(map(get_spread,act,np.roll(act, -1)))

        # p = list(np.roll(at, -1))
        l = list(map(convert_target_to_label , env.target_array_t))
        rt = act * env.target_array_t
        total_test_rewards = rt - st
        # total_test_rewards = env.plot_res(total_test_rewards)
        sum_reward = np.array(total_test_rewards).sum()
        all_sum_reward.append(sum_reward)
        if sum_reward > 0:
            print("------------ run "+str(x)+" is profitalble. profit= "+ str(sum_reward))
            good_run_index.append(x)

        # RandomWalk strategy
        act = np.random.choice([1,-1],1000)
        st = list(map(get_spread,act,np.roll(act, -1)))
        rt = act * env.target_array_t
        random_rewards = rt - st

        # Buy and Hold strategy
        act = np.ones(1000)
        st = list(map(get_spread,act,np.roll(act, -1)))
        rt = act * env.target_array_t
        buyAndHold = rt - st

        # Sell and Hold strategy
        # act = np.ones(1000) * -1
        # st = list(map(get_spread,act,np.roll(act, -1)))
        # rt = act * env.target_array_t
        # sellAndHold = rt - st


        # plot results
        env.plot_balance_p(total_test_rewards, epoch, "test", run_name, random_rewards,buyAndHold) #, sellAndHold)
        env.plot_balance_p(total_train_rewards, epoch, "train", run_name)

        # allrewards = np.reshape(allrewards,-1)
        # env.plot_balance_p(allrewards, 1000, "test", run_name)
all_sum_reward = np.array(all_sum_reward).sum()
print(">>> Overall profit: ",str(all_sum_reward))
print(">>> good runs: ", good_run_index)